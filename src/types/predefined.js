// @flow
import type { AnalysisType } from './analysis';

export type Predefined = {
  analysisTypes: Array<AnalysisType>,
  purposes: Array<any>,
  analysisLabList: Array<any>,
  categories: any,
  sampleTypes: any
};
